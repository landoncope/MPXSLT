//
//  MPXSLTransformServiceProtocol.h
//  MPXSLTransformService
//
//  Created by Markus Piipari on 05/08/15.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^MPXSLTransformCompletionHandler)(NSString *result, NSError *error);


@protocol MPXSLTransformServiceProtocol

@required

- (void)XMLStringFromXMLString:(NSString *)inputXMLString
   byApplyingXSLTransformNamed:(NSString *)transformName
                    parameters:(NSDictionary *)parameters
             completionHandler:(MPXSLTransformCompletionHandler)completionHandler;

@end
