//
//  main.m
//  MPXSLT
//
//  Created by Matias Piipari on 25/05/2017.
//  Copyright © 2017 Manuscripts.app Limited. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
